﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WcfClient.WcfSelfHostedService;

namespace WcfClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var credentials = new ClientCredentials();
            credentials.UserName.UserName = "userName-blabla";
            credentials.UserName.Password = "password-blabla";

            Service1Client wcfClient = new Service1Client();
            ServicePointManager.ServerCertificateValidationCallback = (s, c, ch, e) => true;
            ServicePointManager.Expect100Continue = false;
            wcfClient.ChannelFactory.Endpoint.Behaviors.Remove<ClientCredentials>();
            wcfClient.ChannelFactory.Endpoint.Behaviors.Add(credentials);
            string retstr = wcfClient.GetData(456);
            Console.WriteLine("GetData returns: " + retstr);
            var obj = new DataClassFromService();
            obj.BoolValue = true;
            obj.StringValue = "Hello WCF TCP client!";
            var objret = wcfClient.GetDataUsingDataContract(obj);
            Console.WriteLine("GetDataUsingDataContract returns: " + objret.StringValue);
            Console.ReadLine();
            wcfClient.Close();
        }
    }
}
